# 									Music's Time

## Introduction 

Music's  Time is an application for musique teacher. They can see a student group, share document, manage their time for everything.

For make this application , I use two framework Angular, who can see that on this link

<a>https://gitlab.com/christina.c/angular-chef-d-oeuvre and symfony.</a>



I make this fro my exam.



## Diagram UML



### Use Case



![useCase](./Maquette/useCase.png)



![seeStudent](./Maquette/seeStudent.png)

### Class



![diagrameClass](./Maquette/diagrameClass.png)







## Models



### cellphone

![pagedaccueilTel](./Maquette/pagedaccueilTel.png)



![hamburguer](./Maquette/hamburguer.png)



![techerRegistration](./Maquette/techerRegistration.png)

![diary](./Maquette/diary.png)

![studentRegistration](./Maquette/studentRegistration.png)



![groupChoice](./Maquette/groupChoice.png)

![yearGroup](./Maquette/yearGroup.png)

![studentGroup](./Maquette/studentGroup.png)

![documentSharing](./Maquette/documentSharing.png)



### Computer

![homeOrdi](./Maquette/homeOrdi.png)



![diaryOrdi](./Maquette/diaryOrdi.png)



![GroupChoiceOrdi](./Maquette/GroupChoiceOrdi.png)







## User Stories

- As a Music Teacher, I want to send documents to my students to facilitate the exchange with them.
- As a student I want to have access to documents shared by my teacher to train me before class.
- As a teacher, I want to be able to check my calendar easily in order to better organize my schedule.
- As a music teacher, I want to be able to create groups of students to inform my students of the group in which they are more quickly.
- As a student, I want to be able to consult the schedule in which I am to organize myself afterwards with my other activities.
- As a teacher I want to be able to create groups per year to keep track of all my students and contact them again the following year.
- As a teacher I want to keep track of all documents shared with my students so that I can reuse them with other groups.



### Code



