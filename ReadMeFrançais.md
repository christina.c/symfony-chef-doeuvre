# Musi'c Time

## Introduction

Music's Time est une aplication faite pour les professeurs de musique afin de gérer leur élèves, leurs agenda et l'envoi de document plus facilement.



J'ai créer cette application pour le passage de mon examen de fin de formation le Titre RNCP niveau 3 équivalent au bac +2.

Lors de ce projet j'ai utilisé 2 framework Angular que vous pouvez retrouvez avec ce lien 

<a>https://gitlab.com/christina.c/angular-chef-d-oeuvre</a> et Symfony.

## Diagramme UML



### Cas d'utilisation

![UmlProfesseur](./Maquette/Maquettes Français/UmlProfesseur.png)





![umlEleve](./Maquette/Maquettes Français/umlEleve.png)



### Classe



![diagrammeClasseFrançais](./Maquette/Maquettes Français/diagrammeClasseFrançais.png)



### Maquettes



### Téléphone

![pagedaccueilTel](./Maquette/Maquettes Français/pagedaccueilTel.png)



![menuHamburguer](./Maquette/Maquettes Français/menuHamburguer.png)



![inscriptionProfesseur](./Maquette/Maquettes Français/inscriptionProfesseur.png)

![Agenda](./Maquette/Maquettes Français/Agenda.png)

![inscriptionelevefrannçais](./Maquette/Maquettes Français/inscriptionelevefrannçais.png)

![choixGroupe](./Maquette/Maquettes Français/choixGroupe.png)

![groupeAnnée](./Maquette/Maquettes Français/groupeAnnée.png)

![groupeélèves](./Maquette/Maquettes Français/groupeélèves.png)



![partagedoc](./Maquette/Maquettes Français/partagedoc.png)



### Ordinateur

![accueilOrdi](./Maquette/Maquettes Français/accueilOrdi.png)





![agendaFrançaisOrdi](./Maquette/Maquettes Français/agendaFrançaisOrdi.png)





![choixGroupeFrançais](./Maquette/Maquettes Français/choixGroupeFrançais.png)





# user storie français 

- 1)En tant que Professeur de musique, je veux envoyer des documents à mes élèves pour faciliter l'échange avec eux
- 2)En tant qu'élève je veux avoir accès aux documents partagé par mon professeur pour m'entrainer avant les cours.

- 3)En tant que professeur, je veux pouvoir vérifier mon agenda facilement afin de mieux organiser mon emploi du temps
- 4)En tant que professeur  de musique, je veux pouvoir créer des groupes d'élèves pour informer mes élèves du groupe dans lequel il sont plus rapidement
- 5)En tant qu'élève, je veux pouvoir consulté l'horaire dans laquel je suis pour m'organiser par la suite avec mes autres activités.
- 7)En tant que professeur je veux garder une trace de tout les documents partagé à mes élèves pour pouvoir les réutiliser avec d'autre groupe 



### Code

