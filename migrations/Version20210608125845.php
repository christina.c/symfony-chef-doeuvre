<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210608125845 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE instrument (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE instrument_teacher (instrument_id INT NOT NULL, teacher_id INT NOT NULL, INDEX IDX_C8C5D974CF11D9C (instrument_id), INDEX IDX_C8C5D97441807E1D (teacher_id), PRIMARY KEY(instrument_id, teacher_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE students (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, age VARCHAR(255) NOT NULL, level VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stugroup (id INT AUTO_INCREMENT NOT NULL, teacher_id INT DEFAULT NULL, login VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, INDEX IDX_AFD7928841807E1D (teacher_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE group_students (group_id INT NOT NULL, students_id INT NOT NULL, INDEX IDX_AD59C1CDFE54D947 (group_id), INDEX IDX_AD59C1CD1AD8D010 (students_id), PRIMARY KEY(group_id, students_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE teacher (id INT AUTO_INCREMENT NOT NULL, mail VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE instrument_teacher ADD CONSTRAINT FK_C8C5D974CF11D9C FOREIGN KEY (instrument_id) REFERENCES instrument (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE instrument_teacher ADD CONSTRAINT FK_C8C5D97441807E1D FOREIGN KEY (teacher_id) REFERENCES teacher (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE stugroup ADD CONSTRAINT FK_AFD7928841807E1D FOREIGN KEY (teacher_id) REFERENCES teacher (id)');
        $this->addSql('ALTER TABLE group_students ADD CONSTRAINT FK_AD59C1CDFE54D947 FOREIGN KEY (group_id) REFERENCES stugroup (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_students ADD CONSTRAINT FK_AD59C1CD1AD8D010 FOREIGN KEY (students_id) REFERENCES students (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE instrument_teacher DROP FOREIGN KEY FK_C8C5D974CF11D9C');
        $this->addSql('ALTER TABLE group_students DROP FOREIGN KEY FK_AD59C1CD1AD8D010');
        $this->addSql('ALTER TABLE group_students DROP FOREIGN KEY FK_AD59C1CDFE54D947');
        $this->addSql('ALTER TABLE instrument_teacher DROP FOREIGN KEY FK_C8C5D97441807E1D');
        $this->addSql('ALTER TABLE stugroup DROP FOREIGN KEY FK_AFD7928841807E1D');
        $this->addSql('DROP TABLE instrument');
        $this->addSql('DROP TABLE instrument_teacher');
        $this->addSql('DROP TABLE students');
        $this->addSql('DROP TABLE stugroup');
        $this->addSql('DROP TABLE group_students');
        $this->addSql('DROP TABLE teacher');
    }
}
