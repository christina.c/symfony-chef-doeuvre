<?php

#Angular => creation-group

namespace App\Controller;

use App\Entity\Group;
use App\Form\GroupType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("api/group", name="group")
 */
class GroupController extends AbstractController
{
    /**
     * @Route(methods="GET")
     */
    public function getAll(Request $request){

        $limit = 10;
        $page = 1;

        if($request->get('limit')){
            $limit = $request->get('limit');
        }
        if($request->get('page')) {
            $page = $request->get('page');
        }
        $repo = $this->getDoctrine()->getRepository(Group::class)->findBy(["teacher" => $this->getUser()]);
        return $this->json($repo, Response::HTTP_OK);
    }

    /**
     * @Route("/{login}", methods="GET")
     */
    public function getOne(Group $group)
    {
        
            return $this->json($group);
            
    }

    /**
     * @Route(methods="POST")this.groups = data
     * @Route("/{id}", methods="PATCH")
     * EntityManagerInterface
     * ObjectManager
     */
    public function addGroup(Request $request, EntityManagerInterface $manager, Group $entity = null){
   
        if($entity == null) {
            $entity = new Group();
        }
        $user = $this->getUser();
        
        $form = $this->createForm(GroupType::class, $entity, [
        'csrf_protection' => false
        ]);
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );
        
        if ($form->isSubmitted() && $form->isValid()){
            $entity->setTeacher($user);
            $this->prePersist($entity);
            

            $manager->persist($entity);
            $manager->flush();
            return $this->json($entity, Response::HTTP_CREATED);
        }
        return $this->json($form->getErrors(true), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(int $id, EntityManagerInterface $manager)
    {
        $repo = $this->getDoctrine()->getRepository(Group::class);
        $entity = $repo->find($id);
        if(!$entity) {
            return $this->json(null, Response::HTTP_NOT_FOUND);

        }
        $manager->remove($entity);
        $manager->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    protected function prePersist($entity) {}
}
