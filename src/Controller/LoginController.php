<?php

# Angular => inscription-professeur, login, nav

namespace App\Controller;

use App\Entity\Teacher;
use App\Form\TeacherType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route ("/api", name="api-auth")
 */
class LoginController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/register", methods="POST", name="api_register")
     */
    public function register(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    
    {
        $user = new Teacher();
        $form = $this->createForm(TeacherType::class, $user, array(
            'csrf_protection' => false
        ));
        $form->handleRequest($request);
        $form -> submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $hashedPassword = $encoder->encodePassword($user, $user->getPassword()); 
            $user->setPassword($hashedPassword);

            $manager->persist($user);
            $manager->flush();

            return $this->json($user, Response::HTTP_CREATED);
        }
        return $this->json(["message"=>"utilisateur non enregistré " .$form->getErrors(true) ], Response::HTTP_BAD_REQUEST);
        
    }
    
    /**
     *@Route(methods ="GET") 
     */
    public function currentUser(){
        $user = $this->getUser();
        $user->setPassword("");
        return $this->json($user);
    }
}
