<?php

#Angular => student service, inscription-élève, groupe 

namespace App\Controller;

use App\Entity\Students;
use App\Form\StudentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("api/student", name="student")
 */

class StudentController extends AbstractController
{

    /**
     * @Route(methods="GET")
     */
    public function getAll(Request $request){

        $limit = 22;
        $page = 1;

        if($request->get('limit')){
            $limit = $request->get('limit');
        }
        if($request->get('page')) {
            $page = $request->get('page');
        }
        $repo = $this->getDoctrine()->getRepository(Students::class);
        return $this->json($repo->findBy([], [], $limit, ($page-1)*$limit));
    }

    /**
     * @Route("/{id}", methods="GET")
     */
    public function getOne(int $id)
    {
        $repo = $this->getDoctrine()->getRepository(Students::class);
        $entity = $repo->find($id);
        if($entity){
            return $this->json($entity);
        }
        return $this->json(null, Response::HTTP_NOT_FOUND);
    }

    /**
     * @Route(methods="POST")
     * @Route("/{id}", methods="PATCH")
     */
    public function addStudent(Request $request, EntityManagerInterface $manager, int $id = null){
        $entity = null;
        if($id == null) {
            $entity = new Students();
        } else{
            $repo = $this->getDoctrine()->getRepository(Students::class);
            $entity = $repo->find($id);
            if (!$entity){
                return $this->json(null, Response::HTTP_NOT_FOUND);
            }
        }
        $form = $this->createForm(StudentType::class, $entity, [
        'csrf_protection' => false
        ]);
        $form->submit(
            json_decode($request->getContent(), true),
            false
        );

        if ($form->isSubmitted() && $form->isValid()){
            $this->prePersist($entity);
            $manager->persist($entity);
            $manager->flush();
            return $this->json($entity, Response::HTTP_CREATED);
        }
        return $this->json($form->getErrors(true), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(int $id, EntityManagerInterface $manager)
    {
        $repo = $this->getDoctrine()->getRepository(Students::class);
        $entity = $repo->find($id);
        if(!$entity) {
            return $this->json(null, Response::HTTP_NOT_FOUND);

        }
        $manager->remove($entity);
        $manager->flush();
        return $this->json(null, Response::HTTP_NO_CONTENT);

    }

    protected function prePersist($entity) {}

}


