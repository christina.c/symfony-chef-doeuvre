<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstrumentRepository")
 */
class Instrument
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\teacher", inversedBy="instruments")
     */
    private $instrument;

    public function __construct()
    {
        $this->instrument = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|teacher[]
     */
    public function getInstrument(): Collection
    {
        return $this->instrument;
    }

    public function addInstrument(teacher $instrument): self
    {
        if (!$this->instrument->contains($instrument)) {
            $this->instrument[] = $instrument;
        }

        return $this;
    }

    public function removeInstrument(teacher $instrument): self
    {
        if ($this->instrument->contains($instrument)) {
            $this->instrument->removeElement($instrument);
        }

        return $this;
    }
}
