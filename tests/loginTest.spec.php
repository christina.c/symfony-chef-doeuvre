<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginTest extends WebTestCase{

public function registerTest(){

    $userRegister = json_encode([
        'username' => 'test@test.com',
        'password' => '123456',
        'repeatPassword' => '1234'
    ]);

    $client = static::createClient();

    $client->request('POST', '/api/register', [], [], ['CONTENT_TYPE' => 'application/json'],
    $userRegister);

    json_decode($client->getResponse()->getContent(),true);

    $this->assertResponseStatusCodeSame(200);


    }

public function loginTest(){
    
    $userJson = json_encode([
        'username' => 'test@test.com',
        'password' => '123456'
    ]);

    $client = static::createClient();
    $client -> request('POST', '/api/login_check', [], [], 
    ['CONTENT_TYPE' => 'application/json'],
    'userJson');

    json_decode($client->getResponse()->getContent(),true);
    $this->assertResponseStatusCodeSame(200);
    }
}